# DINAMITE Compiler Docker

To install, run `./get_tarballs.sh` first to gather all the LLVM and Clang source archives.

You will need Docker installed on your system, and the daemon running. Run `docker build .` in the current directory to build the container image. Run `docker images` to find the image identifier of the newly generated image and run it with `docker run -t -i <image_id> /bin/bash` to get to the console.
